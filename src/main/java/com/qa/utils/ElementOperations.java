package com.qa.utils;

import com.qa.base.TestBase;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ElementOperations extends TestBase {


    public void clickElement(WebElement ele) {

        try {

            ele.click();

        } catch (Exception e) {

            System.out.println("Element Not Clickable : " + e);

        }
    }

        public void hoverOnElement(WebElement ele){

        try {
            Actions ac = new Actions(driver);

            ac.moveToElement(ele).build().perform();

        }catch (Exception e){

            System.out.println("Unable to Hover on Element : "+e);
        }
        }

        public void enterText(WebElement ele, String text){

        try {
            ele.sendKeys(text);
        }catch(Exception e){

            System.out.println("Unable to enter text : "+e);
        }


        }

        /*********** -- Wait Methods --- ****************/

    public static void waitForElementToAppear(WebElement ele) throws InterruptedException {

        WebDriverWait waitforElement = new WebDriverWait(driver, 20);
        waitforElement.until(ExpectedConditions.visibilityOf(ele));


    }

}
