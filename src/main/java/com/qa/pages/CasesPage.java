package com.qa.pages;

import com.qa.utils.ElementOperations;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

public class CasesPage extends ElementOperations {

    @FindBy(xpath = "//input[@name='title']")
    private WebElement TITLE_TEXTBOX;

    @FindBy(xpath = "//div[@name='contact']//input[@class='search']")
    private WebElement CONTACT_SEARCHBOX;

    @FindBy(xpath = "//div[@name='contact']//div[contains(@class,'visible menu')]/div")
    private WebElement CONTACT_OPTIONSLIST;


    public CasesPage() {


        PageFactory.initElements(driver, this);

    }

    public void enterTitle(String title) {

        try {
            waitForElementToAppear(TITLE_TEXTBOX);
            enterText(TITLE_TEXTBOX, title);
        } catch (Exception e) {

            System.out.println("Unable to enter the Text : " + e);

        }
    }

    public void selectContact(String text) {

        try {

            enterText(CONTACT_SEARCHBOX, text);
            clickElement(CONTACT_OPTIONSLIST);
            

        } catch (Exception e) {

            System.out.println("Unable to select the Contact : " + e);

        }


    }

}
