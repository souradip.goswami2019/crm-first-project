package com.qa.pages;

import com.qa.base.TestBase;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class GuestHomePage extends TestBase {
    @FindBy(xpath = "//span[text()='Log In']")
    private WebElement Login_btn;

    //Initializing my Page objects
    public GuestHomePage() {

        PageFactory.initElements(driver, this);

    }

    public String validateHomepage() {


        return driver.getTitle();

    }

    public LoginPage navigateToLoginPage() {

        Login_btn.click();
        return new LoginPage();

    }


}
