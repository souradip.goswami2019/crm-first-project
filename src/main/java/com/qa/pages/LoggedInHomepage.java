package com.qa.pages;

import com.qa.base.TestBase;
import com.qa.utils.ElementOperations;
import org.apache.poi.ss.formula.functions.T;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.Objects;

public class LoggedInHomepage extends ElementOperations {



    @FindBy(xpath = "//div[contains(@class,'sidebar')]//i[@class='home icon']")
    private WebElement HOMEPAGE_ICON;

    @FindBy(xpath = "//span[contains(text(),'Cases')]/parent::a//following-sibling::button/i[contains(@class,'plus')]")
    private WebElement CREATECASE_ICON;

    public LoggedInHomepage() {

        
        PageFactory.initElements(driver, this);

    }

    public void hoverOnNavigationIcons() {

       hoverOnElement(HOMEPAGE_ICON);

    }

    public CasesPage navigateToCreateCasepage(){

        clickElement(CREATECASE_ICON);

        return new CasesPage();
    }


//    public Object sidePanelNavigations(String icons) {
//
//        String iconsXpath = "//span[contains(text(),'%s')]".replaceAll("%s", icons);
//
//
////        switch (icons) {
////
////            case "Contacts":
////
////                driver.findElement(By.xpath(iconsXpath)).click();
////                return ContactsPage.class;
////
////
////            case "Companies":
////
////
////                driver.findElement(By.xpath(iconsXpath)).click();
////                return new CompaniesPage();
////
////
////
////
////            default:
////
////                throw new IllegalStateException("Unexpected value: " + icons);
////        }
////
//   }




}


