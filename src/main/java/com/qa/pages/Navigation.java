package com.qa.pages;

import com.qa.base.TestBase;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Navigation extends TestBase {

    @FindBy(xpath = "//div[contains(@class,'sidebar')]//i[@class='home icon']")
    private WebElement HOMEPAGE_ICON;


    public Navigation() {

        PageFactory.initElements(driver, this);

    }

    public void hoverOnNavigationIcons() {

        Actions ac = new Actions(driver);
        ac.moveToElement(HOMEPAGE_ICON).build().perform();

    }


}
