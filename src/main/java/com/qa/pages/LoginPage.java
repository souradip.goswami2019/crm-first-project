package com.qa.pages;

import com.qa.base.TestBase;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LoginPage extends TestBase {

    @FindBy(xpath = "//input[@name='email']")
    private WebElement USERNAME_TXTFIELD;

    @FindBy(xpath = "//input[@name='password']")
    private WebElement PASSWORD_TXTFIELD;

    @FindBy(xpath = "//div[contains(@class,'submit')]")
    private WebElement LOGIN_BTN;

    public LoginPage() {

        PageFactory.initElements(driver, this);

    }

    public LoggedInHomepage loginToCrm(String Username, String Password) {

        USERNAME_TXTFIELD.sendKeys(Username);
        PASSWORD_TXTFIELD.sendKeys(Password);
        LOGIN_BTN.click();

        return new LoggedInHomepage();

    }


}
