package com.qa.base;

import com.qa.utils.TestUtils;
import com.qa.utils.WebEventListener;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

public class TestBase {

    public static WebDriver driver;
    public static Properties properties;
    public static EventFiringWebDriver e_driver;
    public static WebEventListener eventListener;


    public TestBase() {

        //Code to read and fetch properties file
        try {
            properties = new Properties();
            FileInputStream file = new FileInputStream("C:\\Users\\Home\\IdeaProjects\\FreeCRMAutomation\\src\\main\\java\\com\\qa\\config\\config.properties");
            properties.load(file);

        } catch (IOException e) {

            System.out.println("Unable to locate the file");
        }
    }




    //checking the browser name and launching the browser and introducing wait
    public static void initialize() {

        String browserName = properties.getProperty("Browser");
        if (browserName.equals("chrome")) {

            System.setProperty("webdriver.chrome.driver", "C:\\Users\\Home\\IdeaProjects\\FreeCRMAutomation\\src\\main\\resources\\drivers\\chromedriver.exe");
            driver = new ChromeDriver();

        } else if (browserName.equals("Firefox")) {

            System.setProperty("webDriver.gecko.driver", "D:\\Automation\\geckodriver-v0.29.1-win64\\geckodriver.exe");
            driver = new FirefoxDriver();

        }

        e_driver = new EventFiringWebDriver(driver);
        eventListener = new WebEventListener();
        e_driver.register(eventListener);
        driver = e_driver;



        driver.manage().window().maximize();
        driver.manage().timeouts().pageLoadTimeout(TestUtils.PAGE_LOAD_TIMEOUT, TimeUnit.SECONDS);
        driver.manage().timeouts().implicitlyWait(TestUtils.IMPLICIT_WAIT, TimeUnit.SECONDS);

        driver.get(properties.getProperty("Url"));
    }


}
