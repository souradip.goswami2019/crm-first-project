package com.qa.tests;

import com.qa.base.TestBase;
import com.qa.pages.CasesPage;
import com.qa.pages.GuestHomePage;
import com.qa.pages.LoggedInHomepage;
import com.qa.pages.LoginPage;
import com.qa.utils.TestUtils;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class LoginTest extends TestBase {

    public GuestHomePage homepage;
    public LoginPage loginpage;
    public LoggedInHomepage signInHomepage;
    public CasesPage casespage;

    public LoginTest() {

        super();

    }

    @BeforeMethod
    public void setup() {

        initialize();
        homepage = new GuestHomePage();

    }

    @DataProvider
    public Object[][] getCRMTestData(){
        
        Object data[][] = TestUtils.getTestData("LoginPageTestData");
        return data;
    }

    @Test(dataProvider = "getCRMTestData")
    public void homePageNavigationTest(String Username, String Password, String Title, String Contact) throws InterruptedException {

        //Step1 -
        String homepage_title = homepage.validateHomepage();
        Assert.assertEquals(homepage_title, "#1 Free CRM customer relationship management software cloud");
        //Step 2
        loginpage = homepage.navigateToLoginPage();
        //Step 3 -
        signInHomepage = loginpage.loginToCrm(Username,Password);
        signInHomepage.hoverOnNavigationIcons();
        casespage = signInHomepage.navigateToCreateCasepage();
        driver.navigate().refresh();
        //Step 4 -
        casespage.enterTitle(Title);
        casespage.selectContact(Contact);
        

    }


    @AfterMethod
    public void tearDown() {

        driver.manage().deleteAllCookies();
        //driver.close();
    }


}
